from django.db import models
from django.contrib.postgres.fields import JSONField


class Parser(models.Model):
    text = models.CharField(max_length=2000)
    nlu = JSONField(default=dict)
    sql = models.CharField(max_length=2000, null=True, blank=True)
    charts = JSONField(default=dict)

    def __str__(self):
        return "".join(self.text[0:50])


class SalesData(models.Model):
    date = models.DateField()
    country = models.CharField(max_length=200)
    channel = models.CharField(max_length=200)
    product = models.CharField(max_length=200)
    revenue = models.BigIntegerField()
    quantity = models.PositiveIntegerField()

    def __str__(self):
        return "{}-{}".format(self.country, self.product)


