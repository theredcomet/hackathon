import csv

from django.core.management.base import BaseCommand, CommandError
from bot.models import SalesData


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        csv_path = "/code/bot/data/initial_data.csv"
        dataset = []

        if SalesData.objects.count() == 0:
            with open(csv_path, 'rt') as csvfile:
                reader = csv.DictReader(csvfile)
                for row in reader:
                    date = row["date"].split("/")
                    date =  "{}-{}-{}".format(date[2].zfill(4), date[0].zfill(2), date[1].zfill(2))

                    new_obj = SalesData(
                        date=date,
                        country=row["country"],
                        channel=row["channel"],
                        product=row["product"],
                        revenue=row["revenue"],
                        quantity=row["quantity"])

                    dataset.append(new_obj)
            
            SalesData.objects.bulk_create(dataset)