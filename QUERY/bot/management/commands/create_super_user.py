from django.contrib.auth.models import User
from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        if User.objects.count() == 0:
            username, email, password = "abhishek", "abhishek.kulkarni@galepartners.com", "ragstoriches"

            User.objects.create_superuser(username=username, email=email, password=password)
