from .models import SalesData, Parser

from rest_framework import routers, serializers, viewsets

class SalesDataSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = SalesData
        fields = "__all__"


class SalesDataViewSet(viewsets.ModelViewSet):
    queryset = SalesData.objects.all()
    serializer_class = SalesDataSerializer


class ParserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Parser
        fields = "__all__"

class ParserViewSet(viewsets.ModelViewSet):
    queryset = Parser.objects.all()
    serializer_class = ParserSerializer