from django.contrib import admin
from .models import SalesData

class SalesDataAdmin(admin.ModelAdmin):
    list_display = ("date", "country", "channel", "product", "revenue", "quantity")

admin.site.register(SalesData, SalesDataAdmin)


