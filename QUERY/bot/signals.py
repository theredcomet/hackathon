import requests
from . models import SalesData, Parser

from django.db.models import Sum
from django.db.models.signals import post_save
from django.dispatch import receiver

MONTHS = {
    "01": "January",
    "02": "February",
    "03": "March",
    "04": "April",
    "05": "May",
    "06": "June",
    "07": "July",
    "08": "August",
    "09": "September",
    "10": "October",
    "11": "November",
    "12": "December"
}

DIMENSIONS = [
    "country",
    "channel",
    "product"
]

METRIC = [
    "Revenue",
    "metric",
    "quantity"
]

DATE = [
    "builtin.datetimeV2.daterange"
]

DIMENSIONS_ALL = ["Dimension"]

def month_from_date(date_str):
    date_ = str(date_str).split("-")
    return date_[1]

class ParseEntities:
    def __init__(self, entities):
        self.entities = entities
        self.start_date, self.end_date = None, None
        self._set_date_range(DIMENSIONS_ALL)
        self.dims = self._filter_type(DIMENSIONS)
        self.metrics = self._filter_type(METRIC)
        self.wdims = self._filter_type(DIMENSIONS_ALL)

        self.dims_val = self._filter_value(DIMENSIONS)
        self.metrics_val = self._filter_value(METRIC)
        self.wdims_val = self._filter_value(DIMENSIONS_ALL)

    def _filter_type(self, types, label="type"):
        return [ent[label] for ent in self.entities if ent["type"] in types]

    def _filter_value(self, types):
        values = []
        for ent in self.entities:
            if ent["type"] in types:
                ent_ = ent["resolution"]["values"]
                values += ent_
        return values

    def _set_date_range(self, types):
        for ent in self.entities:
            if ent["type"] in types:
                try:
                    ent_ = ent["resolution"]["values"][0]
                    self.start_date, self.end_date = ent_["start"], ent_["end"]
                except Exception:
                    pass
        if not self.start_date or self.end_date:
            self.start_date, self.end_date = "2018-09-06", "2019-09-06"

    def _ulen(self, data):
        return len(set(data))

    def _is_tree_map(self):
        if self._ulen(self.wdims) == 0 and self._ulen(self.dims) == 1 and self._ulen(self.metrics) == 1:
            if self._ulen(self.dims_val) > 1 :
                return True
            return False
        if self._ulen(self.wdims) == 1 and self._ulen(self.dims) == 0 and self._ulen(self.metrics) == 1:
            if self._ulen(self.dims_val) > 1 :
                return True
            return False

        return False

    def _is_category_stack(self):
        if self._ulen(self.dims) == 1 and self._ulen(self.wdims) == 0 and self._ulen(self.metrics) == 1:
            return True
        if self._ulen(self.dims) == 0 and self._ulen(self.wdims) == 1 and self._ulen(self.metrics) == 1:
            return True
        return False

    def get_category_map(self):
        if self._is_category_stack():
            kwargs = dict()
            group_by = [list(set(self.dims))[0], "date"]
            annotate = self.metrics_val[0].lower()

            kwargs = {
                    "{}__in".format(group_by[0]): list(set(self.dims_val)),
                    "{}__range".format("date"): (self.start_date, self.end_date)
                }
            
            data = SalesData.objects.filter(**kwargs)\
                .values(*group_by)\
                .annotate(revenue=Sum(annotate))                

            query = data.query.__str__()
            legends = list(set([d[group_by[0]] for d in data]))
            months = list(set([month_from_date(d[group_by[1]]) for d in data]))
            values = []
            for leg in legends:
                for mon in months:
                    obj_ = {
                        "name": leg,
                        "data": SalesData.objects.filter(country__in=[leg], date__month=mon).values(annotate).annotate(Sum(annotate))}

                    values.append(obj_)

            return query
            # keys = data[0].keys()
            # keys = [k for k in keys if k not in ["_state", "id"]]
            # data_ = []

            # for k in keys:
            #     flat_ = {"name": k, "data": [d[k] for d in data]}
            #     data_.append(flat_)

            # return data_
        return None

    def get_tree_map(self):
        if self._is_tree_map():
            kwargs = dict()
            group_by = list(set(self.dims))
            annotate = self.metrics_val[0].lower()

            if self._ulen(self.dims_val) > 1:
                print("kwargs: ", list(set(self.dims)))
                kwargs = {
                    "{}__in".format(group_by[0]): list(set(self.dims_val)),
                    "{}__range".format("date"): (self.start_date, self.end_date)
                }

                print(kwargs)

            data = SalesData.objects.filter(**kwargs)\
                .values(*group_by)\
                .annotate(revenue=Sum(annotate))
            kv_f = lambda f: {"name": f[group_by[0]], "value": f[annotate]}
            key_val = [kv_f(obj) for obj in data]
            return data.query.__str__()
        return None



@receiver(post_save, sender=Parser)
def city_replicator(sender, **kwargs):
    """
    Replicates the CityMaster to City (slave) instance
    """
    if kwargs.get("created"):
        data = kwargs.get("instance")
        base_url = "https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/96aa7a75-1dd0-49e3-8cc7-713787c64061?verbose=true&timezoneOffset=-360&subscription-key=efff7641509d4568a880738506916b52&q={}"

        res = requests.get(base_url.format(data.text))
        data.nlu = res.json()

        charts = ParseEntities(data.nlu["entities"])
        tree_map = charts.get_tree_map()
        if tree_map:
            data.sql = tree_map
        elif charts.get_category_map():
            data.sql = charts.get_category_map()

        try:
            data.save()
        except Exception as e:
            print(e)
