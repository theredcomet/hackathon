#!/bin/bash

echo "Installing dependencies..."
python -m spacy download en_core_web_md
python -m spacy link en_core_web_md en

echo "collecting static files"
python manage.py collectstatic --clear --noinput

echo "Apply database migrations"
python manage.py makemigrations
python manage.py migrate

echo "Creating super users"
python manage.py create_super_user

echo "Loading dummy data"
python manage.py load_dummy_data

echo "Starting gunicorn web application server"
gunicorn -b 0.0.0.0:5000 QUERY.wsgi