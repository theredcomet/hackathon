FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
COPY ./requirements.txt /code/
RUN pip install -r requirements.txt

COPY ./QUERY /code/

COPY ./entry-point.sh /code/entry-point.sh


RUN chmod +x /code/entry-point.sh
ENTRYPOINT ["/code/entry-point.sh"]

EXPOSE 5000
